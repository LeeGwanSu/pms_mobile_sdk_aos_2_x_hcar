package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.DataKeyUtil;

public class GetConfig extends BaseRequest {

	public GetConfig(Context context) {
		super(context);
	}

	public JSONObject getParam (String custId, String loginState) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("loginState", loginState);
			jobj.put("custId", custId);
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String custId, String loginState, final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_CONFIG, getParam(custId, loginState), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			DataKeyUtil.setDBKey(mContext, DB_MSG_FLAG, json.getString("msgFlag"));
			DataKeyUtil.setDBKey(mContext, DB_NOTI_FLAG, json.getString("notiFlag"));
			DataKeyUtil.setDBKey(mContext, DB_MKT_FLAG, json.getString("mktFlag"));
			DataKeyUtil.setDBKey(mContext, DB_REMOTE_FLAG, json.getString("remoteFlag"));
			DataKeyUtil.setDBKey(mContext, DB_SERVICE_FLAG, json.getString("serviceFlag"));
			DataKeyUtil.setDBKey(mContext, DB_NOTICE_FLAG, json.getString("noticeFlag"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
