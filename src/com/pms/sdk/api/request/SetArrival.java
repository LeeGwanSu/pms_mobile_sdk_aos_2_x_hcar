package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;

public class SetArrival extends BaseRequest {

	public SetArrival(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String msgid) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("uuid", PMSUtil.getUUID(mContext));
			jobj.put("msgId", msgid);

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String msgId, final APICallback apiCallback) {
		try {
			apiManager.call(API_SET_ARRIVAL, getParam(msgId), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
