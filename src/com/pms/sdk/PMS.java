package com.pms.sdk;

import java.io.Serializable;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 *          [2015.02.04 10:23] SDK 사용 중지 요청 가능하게 수정함. <br>
 *          [2015.02.05 19:33] newMsg Update시 DEL_YN값이 null값이 들어가는 현상 수정함. <br>
 *          [2015.02.11 09:55] Flag 값들이 안바뀌는 현상 수정함. <br>
 *          [2015.02.25 11:45] 앱 최초설치시에는 msgFlag & notiFlag 값 서버에서 받아온 값 저장함. <br>
 *          [2015.03.03 16:55] 앱실행시 체크하는 프로세스 롤리팝 버전에 맞게 수정함.<br>
 *          [2015.03.11 10:02] Private 서버 안정화.<br>
 *          [2015.03.17 18:11] 팝업창 노출 플래그 추가함.<br>
 *          [2015.03.19 16:29] DB쪽 버그 수정함.<br>
 *          [2015.03.30 21:34] 팝업 클릭시 ClickMsg & readMsg 직접호출로 수정함.<br>
 *          [2015.04.03 09:40] Notification Image Load가 실패하면 Text Notification으로 전환하는 코스 삽입함.<br>
 *          [2015.04.23 16:59] GCM 받아오는 부분 팝업창 뜨도록 되어 있는부분을 로그로 찍게 수정함.<br>
 *          [2015.06.12 10:21] GCM 미지원시 오류 처리함.<br>
 *          [2015.06.26 10:50] LogoutPms.m 호출시 MaxUserMsgId를 -1로 초기화 하는 루틴 수정함.<br>
 *          [2015.07.20 13:23] Notification Priority추가함.<br>
 *          [2015.07.20 16:43] BigImage시 메세지 내용도 보이게 수정함.<br>
 *          [2015.09.07 10:58] 팝업창 뛰우는 플래그 'N'으로 변경. MSG class pushImg 값 추가함.<br>
 *          [2015.10.13 15:35] Android 6.0 접근권한 체크하는 루틴 추가함.<br>
 *          [2015.10.21 13:53] Logout시 Max user Msg Id값 초기화 하는 루틴 추가.<br>
 *          [2015.12.30 14:05] MsgGrpCd가 문자열일때 오류나는 부분 수정함.<br>
 *          [2016.01.15 14:17] mktFlag 값 추가함.<br>
 *          [2016.02.25 16:54] MktFlag 정책 변경으로 인한 적용<br>
 *          [2016.03.08 13:26] Logout시에도 적용되도록 변경함.<br>
 *          [2016.03.21 14:21] selfsign을 없애고 getSignKey에서 SSL관련 개인보안키를 받도록 수정함.<br>
 *          [2016.04.29 13:23] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.06.15 15:11] Login Flag값 초기화 되는 값 삽입.<br>
 *          [2016.06.16 17:05] 신규 API적용함. <br>
 *          [2016.06.29 10:55] SetConfig Flag 3가지 추가함. Select API 추가함. <br>
 *          [2016.07.22 17:32] GetConfig API 추가함. <br>
 *          [2016.07.29 09:31] DevicCert시 SetConfig값들 저장하지 않도록 수정함.<br>
 *          [2016.08.04 14:03] LoginState가 CustID가 유뮤에 따라에 변하도록 수정함.<br>
 *          [2016.08.08 18:02] extLink 값 빠진부분 수정함.<br>
 *          [2016.08.12 14:16] SelectMsgList에서 msgGrpCode값을 배열로 넘기도록 수정함.<br>
 *          [2016.08.18 09:17] readMsg 호출시 MsgGrpCd를 다중으로 넣을 수 있도록 수정함.<br>
 *          [2016.12.02 18:36] doze mode broadcast 패치1 적용 <br>
 *          [2017.06.09 17:20] doze mode broadcast 패치1 적용, background/foreground 상태에 따라 showNotification 처리 추가<br>
 *          [2017.06.16 16:52] background/foreground 상태에 따라 showNotification 처리 원복<br>
 *          [2017.10.30 17:08] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					* DB 중복 Open으로 인해 강제종료 문제 수정
 *          					* PushPopup 발생 시 강제 종료 문제 수정
 *          					* UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					  UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함 <br>
 *          [2018.09.21 12:38] Android 8.0 대응, 잠금화면체크, 벨소리 볼륨 조절, 암시적 Intent, 이미지캐시 대응
 *          [2018.10.10 10:14] 벨소리 채널 볼륨 대응
 *          [2018.10.16 17:55] Wear 디바이스 대응
 *          [2018.10.30 01:12] 현대자동차 요청으로 NewMsg에 -1 못넣게함, Cursor 에러처리 추가
 *          [2018.12.19 15:36] AppUserId를 조회하여 중복 메시지 피함, FCM 적용
 *          [2019.04.12 11:22] 벨소리 볼륨 로직 제거, 비동기 토큰 발급 후 DeviceCert 요청하도록 변경
 *          [2019.08.01 09:55] 알림 확장 기능 추가
 *          [2019.08.19 11:41] MQTTBinder 적용, DateUtil kk->HH 적용
 *          [2019.10.11 09:25] 채널 재생성 로직 제거, MsgDB 메소드 추가, msgGrpCode -1 넣어도 되게함
 *          [2019.11.07 16:22] Android 10 IMEI 값 못가져오는 이슈 대응하기 위해 DeviceCert에 AndroidId 파라미터 추가함
 *          [2019.11.12 15:20] DeviceCert API 호출후 UUID 업데이트 하는 로직 제거
 *          [2020.10.28 10:49] Android X 대응
 */
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;

	private static PMS instancePms = null;

	private static PMSPopup instancePmsPopup = null;

	private Context mContext = null;

	private PMSDB mDB = null;

	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:202401161051");

		initOption(context);
	}

	public static PMS getInstance (Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		CLog.setDebugMode(context);
		CLog.setDebugName(context);
		if (instancePms == null) {
			instancePms = new PMS(context);
		}
		instancePms.setmContext(context);

		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void initOption (Context context) {
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_RING_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_ALERT_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_ALERT_FLAG, "N");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_MAX_USER_MSG_ID))) {
			DataKeyUtil.setDBKey(context, DB_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_LOGINED_STATE))) {
			DataKeyUtil.setDBKey(mContext, DB_LOGINED_STATE, FLAG_N);
		}
	}

	public void setCustId (String custId) {
		PMSUtil.setCustId(mContext, custId);
	}

	public String getCustId () {
		return PMSUtil.getCustId(mContext);
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	public void setNotiOrPopup (Boolean isnotiorpopup) {
		PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
	}

	public void setRingMode (boolean isRingMode) {
		DataKeyUtil.setDBKey(mContext, DB_RING_FLAG, isRingMode ? "Y" : "N");
	}

	public void setVibeMode (boolean isVibeMode) {
		DataKeyUtil.setDBKey(mContext, DB_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	public void setPopupNoti (boolean isShowPopup) {
		DataKeyUtil.setDBKey(mContext, DB_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	public void setPushToken(String pushToken) {
		CLog.d("setPushToken:pushToken=" + pushToken);
		PMSUtil.setGCMToken(mContext, pushToken);
	}

	public String getMsgFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG);
	}

	public String getNotiFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG);
	}

	public String getMktFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG);
	}

	public String getRemoteFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_REMOTE_FLAG);
	}

	public String getServiceFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_SERVICE_FLAG);
	}

	public String getNoticeFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_NOTICE_FLAG);
	}

	public String getMaxUserMsgId () {
//		return DataKeyUtil.getDBKey(mContext, DB_MAX_USER_MSG_ID);
		return mDB.selectLastUserMsgId();
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row, String... msgCode) {
		return mDB.selectMsgList(page, row, msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */
	public void setBridgeTag(Context context, String tag)
	{
		DataKeyUtil.setDBKey(context, DB_BRIDGE_TAG, tag);
	}
	public String getBridgeTag(Context context)
	{
		String tag = DataKeyUtil.getDBKey(mContext, DB_BRIDGE_TAG);
		if(tag!=null && tag.length() > 0)
		{
			return tag;
		}
		else
		{
			return DEFAULT_BRIDGE_TAG;
		}
	}
	public int getAllOfUnreadCount() {return mDB.getAllOfUnreadMsgCount();}

	public int getUnreadCountByMsgGrpCode(String msgGrpCode) { return mDB.getUnreadMsgCountByMsgGrpCode(msgGrpCode);}

	public Cursor selectAllOfMsg () {
		return mDB.selectAllOfMsg();
	}
	public Cursor selectAllOfMsgByMsgGrpCode (String msgGrpCode) {
		return mDB.selectAllOfMsgByMsgGrpCode(msgGrpCode);
	}
	public Cursor selectAllOfMsgGrp () {
		return mDB.selectAllOfMsgGrp();
	}
}
